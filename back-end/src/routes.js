const express = require('express');

const routes = express.Router();

const OngController = require('./controllers/OngController');
const IncidentController = require('./controllers/IncidentController');
const ProfileController = require('./controllers/ProfileController');
const SessionController = require('./controllers/SessionController');

/**
 * Rotas e Recursos
 * GET: Buscar/Listar
 * POST : Inserir
 * PUT : Alterar
 * DELETE : Excluir
 */

 /**
  * Tipos de parâmetros:
  * Query Params : Parâmetros nomeados enviados na rota após "?" (filtro, paginação).
  * Route Params : Parâmetros utilizados para identificar recursos
  * Request Body : Corpo da requisição, utilizado para criar ou alterar recursos.
  */

 routes.post('/sessions', SessionController.create);

 routes.post('/ongs', OngController.create);

routes.get('/ongs', OngController.index);

routes.post('/incidents', IncidentController.create);

routes.get('/incidents', IncidentController.index);

routes.delete('/incidents/:id', IncidentController.delete);

routes.get('/profiles', ProfileController.index);

module.exports = routes;
