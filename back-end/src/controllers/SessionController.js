const conn = require('../database/connection');

module.exports = {
    async create(req, res) {
        const { id } = req.body;

        if(!id) {
            return res.status(400).send('O ID deve ser informado.');
        }

        const ong = await conn('ongs')
        .where('id', id)
        .select('name')
        .first();

        if(!ong) {
            return res.status(400).send('No ONG found with this ID.');
        }
        return res.json(ong);
    }
}