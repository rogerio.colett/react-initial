const conn = require('../database/connection');

module.exports = {
    async index(req, res) {

        const { page = 1 } = req.query;

        const [count] = await conn('incidents').count();

        const incidents = await conn('incidents')
        .join('ongs', 'ongs.id', '=', 'incidents.ong_id')
        .limit(5)
        .offset((page -1) * 5)
        .select([
            'incidents.*',
            'ongs.name',
            'ongs.email',
            'ongs.whatsapp',
            'ongs.city',
            'ongs.uf'
        ]);

        res.header('X-Total-Count', count['count(*)'])
    
        return res.json(incidents);
    },
    async create(req, res) {

        try{
            // recebe o valor das variáveis
            const {title, description, value} = req.body;

            const ong_id = req.headers.authorization;

            // #region Validation
            if(!ong_id || ong_id.length != 12) {
                res.status(401).send('Usuário não autorizado!');
                return;
            } else if(!title) {
                res.status(400).send('O título do caso deve ser informado!');
                return;
            } else if(!description) {
                res.status(400).send('A descrição do caso deve ser informada!');
                return;
            } else if(!value || isNaN(value)) {
                res.status(400).send('O valor do caso deve ser informado!');
                return;
            } 
            // #endregion
            
            const [id] = await conn('incidents').insert({
                title, description, value, ong_id
            });

            return res.json({ message : 'Caso inserido com sucesso', id : id })
        } catch(ex) {
            res.status(400).send(ex.message || ex);
        }
    },

    async delete(req, res) {

        const { id } = req.params;

        const ong_id = req.headers.authorization;
    
        const incident = await conn('incidents')
            .where('id', id)
            .select('ong_id')
            .first();
        if (!incident || incident.ong_id != ong_id) {
            return res.status(401).json({ error: 'Operation not permitted.' })
        }

        await conn('incidents').where('id', id).delete();

        return res.status(204).send('Successful operation.');
    }
}