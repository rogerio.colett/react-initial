const connection = require('../database/connection');
const crypto = require('crypto');

module.exports = {

    async index(req, res) {

        const ongs = await connection('ongs').select('*');
    
        return res.json(ongs);
    },

    async create(req, res) {
        
        const { name, email, whatsapp, city, uf } = req.body;

        // #region Validation
        if(!name) {
            res.status(400).send('O nome da ONG deve ser informado!');
            return;
        } else if(!email) {
            res.status(400).send('O e-mail da ONG deve ser informado!');
            return;
        } else if(!whatsapp) {
            res.status(400).send('O contato via whatsapp da ONG deve ser informado!');
            return;
        } else if(!city) {
            res.status(400).send('A cidade de localização da ONG deve ser informada!');
            return;
        } else if(!uf) {
            res.status(400).send('O Estado da localização da ONG deve ser informado!');
            return;
        }
        // #endregion
        
        const id = crypto.randomBytes(6).toString('HEX').toUpperCase();
        

        await connection('ongs').insert({
            id, name, email, whatsapp, city, uf
        })

        return res.json({ message : 'ONG cadastrada com sucesso', id : id });

    }
}